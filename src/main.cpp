//
// Created by Piotr Szewczuk on 2019-02-10.
//

#include <QtCore>
#include <QCoreApplication>



int main(int argc, char *argv[])
{
    qInfo() << "app starts";

    QCoreApplication a(argc, argv);

    return a.exec();
}
