#
# Created by Piotr Szewczuk on 2019-02-10.
#

project ( ui )


###############################
###          FILES         ####
###############################

set ( SRC  )


###############################
###          TARGET        ####
###############################

add_library ( ${PROJECT_NAME} ${SRC} )

target_link_libraries ( ${PROJECT_NAME} PUBLIC
    Qt5::Core )

target_include_directories ( ${PROJECT_NAME} PUBLIC
    ${PROJECT_SOURCE_DIR}/ )